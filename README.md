# IOS TESTY NAVOD:

## 1.MOZNOST(ZRIADENIE AUTOMATICKYCH TESTOV NA GITLABE)

### Vytvorenie prostredia:

    1.Vytvorte si novy repo na gitlabe a podla navodu na gitlabe nahrajte vas projekt na gitlab.

    2.Stiahnite si tento repo a do suboru s vasim projektom nakopirujte len viditilne subory
        (subory ktore neobsahuju "." na zaciatku nazvu)

    3.Pridajte tieto subory do repozitara(teraz vzdy pri dalsiom commite sa automaticke testy spustia)

### Zobrazenie logov z automatickych testov:
    1.Otvorte si vas repozitar.

    2.V lavom menu najdite CI/CD ukazte na tlacitko mysou a stlacte tlacitko pipelines vo vyskakovaciom menu.

    3.Kliknite na tlacitko failed/passed v poslednom "jobe" 
        pre zobrazenie logov z posledneho automatickeho testu.(Poznamka: posledny job je prvy v zozname.)
    4.Vyberte si OS z ktoreho si chcete pozriet logy a kliknite nan.
    
    5.Logy sa zobrazia na obrazovke.

## 2.MOZNOST(OTESTOVANIE NA MERLINOVI/EVE)

    1.Presunte na preferovany server svoj subor s projektom.

    2.Otvorte subor s projektom a stiahnite tento repo.
    
    3.spustite skript: ./run_tests
    
